<?php

class PokemonApi
{
    private const BASE_URL = 'https://pokeapi.co/api/v2/pokemon';

    public function getAllPaginated(int $offset): string
    {
        try {
            $data = $this->apiCall(self::BASE_URL . '?limit=100&offset=' . $offset);
            $html_rows = '';
            foreach ($data['results'] as $row) {
                $html_rows = $html_rows . $this->convertToHtmlRow($row['name'], $row['url']);
            }
            return $html_rows;
        } catch (Exception $e) {
            return '';
        }
    }

    public function getByName(string $name): string
    {
        if ($name === '') {
            return $this->getAllPaginated(0);
        }
        try {
            $data = $this->apiCall(self::BASE_URL . '/' . $name);
            return $this->convertToHtmlRow($data['name'], self::BASE_URL . '/' . $data['id']);
        } catch (Exception $e) {
            return '';
        }
    }

    public function getByUrl(string $url): array
    {
        try {
            $data = $this->apiCall($url);
            $abilities = [];
            foreach ($data['abilities'] as $ability) {
                array_push($abilities, $ability['ability']['name']);
            }
            return [
                'name' => $data['name'],
                'image' => $data['sprites']['front_default'],
                'height' => $data['height'],
                'weight' => $data['weight'],
                'abilities' => $abilities
            ];
        } catch (Exception $e) {
            return [];
        }
    }

    private function convertToHtmlRow($name, $url) {
        return '<tr><td>' . $name . '</td><td><form method="POST" action="">
                    <input type="hidden" name="url" value="' . $url . '" />
                    <button class="btn btn-primary" type="submit">Go to</button>
                </form></td></tr>';
    }

    private function apiCall(string $url): array
    {
        $handle = curl_init();

        curl_setopt($handle, CURLOPT_URL, $url);
        curl_setopt($handle, CURLOPT_POST, false);
        curl_setopt($handle, CURLOPT_HEADER, true);
        curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($handle, CURLOPT_CONNECTTIMEOUT, 10);

        $response = curl_exec($handle);
        $hlength = curl_getinfo($handle, CURLINFO_HEADER_SIZE);
        $httpCode = curl_getinfo($handle, CURLINFO_HTTP_CODE);
        $body = substr($response, $hlength);

        // If HTTP response is not 200, throw exception
        if ($httpCode != 200) {
            throw new Exception($httpCode);
        }

        return json_decode($body, true);
    }
}