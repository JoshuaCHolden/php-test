<?php
require '../vendor/autoload.php';
$pokemon_api = new PokemonApi();

if (isset($_GET['url'])) {
    $this_pokemon = $pokemon_api->getByUrl($_GET['url']);
} else {
    http_response_code(404);
    header('Location: /404.html');
}

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">

    <title>Pokédex</title>
</head>
<body>
<div style="display: flex; flex-direction: column; align-items: center">
    <h1 class="">
        <?php echo($this_pokemon['name']); ?>
    </h1>

    <img src="<?php echo($this_pokemon['image']); ?>" class="img-fluid" alt="Responsive image">

    <p>Height: <?php echo($this_pokemon['height']); ?></p>
    <p>Weight: <?php echo($this_pokemon['weight']); ?></p>
    <h4>Abilities</h4>
    <?php
    foreach ($this_pokemon['abilities'] as $ability) {
        echo('<div>' . $ability . '</div>');
    }
    ?>

    <a href="./index.php" class="btn btn-primary stretched-link">back</a>

</div>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4"
        crossorigin="anonymous"></script>
</body>
</html>