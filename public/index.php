<?php
require '../vendor/autoload.php';
$pokemon_api = new PokemonApi();

if (isset($_POST['url'])) {
    header("Location: /pokemon.php?url=".$_POST['url']);
}
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">

    <title>Pokédex</title>
</head>
<body>
<table class="table">
    <thead>
    <tr>
        <th scope="col">Name</th>
        <th scope="col"></th>
    </tr>
    </thead>
    <tbody>
    <tr>

        <form action="" method="post">
            <td>
                <div class="mb-3">
                    <input type="text" name="name" class="form-control" id="name" aria-describedby="pokemonName" placeholder="filter">
                </div>
            </td>
            <td><button type="submit" class="btn btn-primary">Search</button></td>
        </form>

    </tr>
    <?php

        if (isset($_POST['name'])) {
            echo($pokemon_api->getByName($_POST['name']));
        } else {
            echo($pokemon_api->getAllPaginated(0));
        }
    ?>
    </tbody>
</table>


<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4"
        crossorigin="anonymous"></script>
</body>
</html>